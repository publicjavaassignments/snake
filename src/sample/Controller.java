package sample;

import javafx.animation.Animation;
import javafx.animation.AnimationTimer;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.util.*;

public class Controller {

    private int speed = 10;
    private int snakeSize = 50;
    private boolean gameOver = false;
    private boolean isFlipped = false;
    private static List<snakePositions> snake = new ArrayList<>(); // Arraylist to track SnakePositions objects
    // Tile sizes
    private int tileWidth = 15; // Offset because 0,0 is also a valid coordinate
    private int tileHeight = 15; // Offset because 0,0 is also a valid coordinate
    // Food Positions
    private int foodX = 0;
    private int foodY = 0;
    // Default movement vector
    private movementDirection direction = movementDirection.right;
    // Timers
    private Runnable mTask;
    private TimerTask mTimerTask;
    // Score
    private int score = 0;
    private boolean hasStarted = false;
    private GraphicsContext gc;
    @FXML
    private Canvas canvasBoard;

    public void gameLoop(GraphicsContext gc) {
        if (gameOver) {
            gc.setFill(Color.WHITE);
            gc.fillRect(0, 0, 800, 800);

            gc.setFill(Color.BLACK);
            gc.setFont(new Font("", 60));
            gc.fillText("You died!", 275, 400);
            gc.setFont(new Font("", 60));
            gc.fillText("Final score: " + score, 230, 450);
        } else {

            for (int i = snake.size() - 1; i >= 1; i--) {
                snake.get(i).x = snake.get(i - 1).x;
                snake.get(i).y = snake.get(i - 1).y;
            }


            // Switchcase to handle directions, if statements handle warping
            switch (direction) {
                case up:
                    snake.get(0).y--;
                    if (snake.get(0).y < 0) {
                        snake.get(0).y = tileHeight;
                    }
                    break;
                case down:
                    snake.get(0).y++;
                    if (snake.get(0).y > tileHeight) {
                        snake.get(0).y = 0;
                    }
                    break;
                case left:
                    snake.get(0).x--;
                    if (snake.get(0).x < 0) {
                        snake.get(0).x = tileWidth;
                    }
                    break;
                case right:
                    snake.get(0).x++;
                    if (snake.get(0).x > tileWidth) {
                        snake.get(0).x = 0;
                    }
                    break;
            }

            if (foodX == snake.get(0).x && foodY == snake.get(0).y) {
                snake.add(new snakePositions(-1, -1));
                score++;
                placeFood();

                Random rotationChance = new Random();

                int rotation = rotationChance.nextInt(100);

                // Random flips
                if (score > 10 && rotation <= 10 && isFlipped == false) {
                    isFlipped = true;
                    canvasBoard.setRotate(90);
                }
            }

            //Call baggrund redraw!
            drawBoardBackground();

            for (int i = 1; i < snake.size(); i++) {
                if (snake.get(0).x == snake.get(i).x && snake.get(0).y == snake.get(i).y) {
                    gameOver = true;
                }
            }

            // Draw foodies
            Image image = new Image("sample/resources/strawberry.png");
            gc.drawImage(image, (foodX * 50), (foodY * 50));

            // Draw snek!
            for (snakePositions longboi : snake) {
                gc.setFill(Color.DARKRED);
                gc.fillOval(longboi.x * snakeSize, longboi.y * snakeSize, snakeSize - 2, snakeSize - 2);
            }
            // Score
            gc.setFill(Color.BLACK);
            gc.setFont(new Font("", 30));
            gc.fillText("Score: " + (score), 10, 30);
        }
    }

    public void initialize() {
        gc = canvasBoard.getGraphicsContext2D();
        drawBoardBackground();

        gc.setFill(Color.BLACK);
        gc.setFont(new Font("", 60));
        gc.fillText("Press space to start!", 115, 400);

        // Initial snake parts
        snake.add(new snakePositions(7, 7));
        snake.add(new snakePositions(7, 7));
        snake.add(new snakePositions(7, 7));

        Stage importedStage = Main.getPrimaryStage();
        importedStage.close();

        importedStage.addEventFilter(KeyEvent.KEY_PRESSED, key ->
        {
            if (key.getCode() == KeyCode.SPACE && hasStarted == false) {
                hasStarted = true;

                Timeline timeline = new Timeline(
                        new KeyFrame(Duration.seconds(7), e -> {
                            placeFood();
                        })
                );
                timeline.setCycleCount(Animation.INDEFINITE);
                timeline.play();

                new AnimationTimer() {
                    long lastTick = 0;

                    public void handle(long now) {
                        if (lastTick == 0) {
                            lastTick = now;
                        }

                        if (now - lastTick > 1000000000 / speed) {
                            lastTick = now;
                            gameLoop(gc);
                        }
                    }
                }.start();
            }
        });

        importedStage.addEventFilter(KeyEvent.KEY_PRESSED, key ->
        {
            if (key.getCode() == KeyCode.W || key.getCode() == KeyCode.UP) {
                direction = movementDirection.up;
            }
            if (key.getCode() == KeyCode.A || key.getCode() == KeyCode.LEFT) {
                direction = movementDirection.left;
            }
            if (key.getCode() == KeyCode.S || key.getCode() == KeyCode.DOWN) {
                direction = movementDirection.down;
            }
            if (key.getCode() == KeyCode.D || key.getCode() == KeyCode.RIGHT) {
                direction = movementDirection.right;
            }

        });
    }

    public void placeFood() {


        Random rand = new Random();

        foodX = rand.nextInt(15);
        foodY = rand.nextInt(15);
    }

    public void drawBoardBackground() {

        gc.setFill(Color.WHITE);
        gc.fillRect(0, 0, 800, 800);

        gc.setStroke(Color.web("#999", 0.5));

        for (int i = 0; i < 800; i += 50) {
            gc.strokeLine(i, 0, i, 800);
        }
        for (int i = 0; i < 800; i += 50) {
            gc.strokeLine(0, i, 800, i);
        }
        gc.strokeRect(0, 0, 800, 800);
    }

    // Movement directions
    public enum movementDirection {
        left, right, up, down
    }

    public static class snakePositions // As object
    {
        int x;
        int y;

        public snakePositions(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }
}
